#include "LBTimer.hpp"
#include <iostream> // para cout 
int main()
{
  // se compila agregando el uso del c++1: g++ ejemplo.cpp -std=c++11 
  
  // uso del timer sin lattice, esto podria usarse para tomar tiempos de cualquier codigo
  
  LBTimer lbtimer; // creo un objeto de la clase LBTimer
  lbtimer = LBTimer(); // ps lo creo 
  lbtimer.startTiming(); // aqui empiezo el reloj

  double d = 0;   //  Aqui pongo las funciones o procesos de los que quiera 
  // tomar el tiempo 
  for(int i = 0; i < 10e6 ; i++){
    d += 0.01;
  }
  double t = lbtimer.stopTiming(); // la funcion stopTiming() ; para el reloj y retorna los segundos empleados en el calculo. 
  std::cout <<  t << std::endl;

  // Ahora un ejemplo considerando un lattice. Imaginermos que tenemos un codigo LB de tamaño Lx Ly y que queremos medir el rendimiento el LUPS ( lattices actualizados por segundo) procedemos asi:

  int Lx = 50, Ly = 50 , pasos = 100; // 50x50 y 100 pasos de tiempo.
  LBTimer lbtimer2( Lx, Ly , pasos); // creo el timer
  lbtimer2.startTiming();
  // aqui va el ciclo del lattice, el for con los pasos de adveccion y colission
  
  for ( int i = 0 ; i < pasos*1e6 ; i++){ /// bucle ficticio solo para mostrar la idea
    d -= 0.1;
  }
  double lups = lbtimer2.stopTiming(); // lups calculados ! asi de facil. 
  std::cout << lups << std::endl;
  // y tambien se puede utilizar en un Lattice3D . 
  
    
}
